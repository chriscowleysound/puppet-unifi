# == Class: unifi::params
#
# Sets the default parameters for the module
#
class unifi::params {
  $ensure       = 'latest'
  $package_name = 'unifi'
  $java_package = 'openjdk-8-jdk'
  $install_mongodb = true
}

# Class unifi::install

class unifi::install {
  if "${::osfamily}" == 'Debian' { # lint:ignore:only_variable_string
    ensure_resources( 'package', $::unifi::java_package, { 'ensure' => 'present' })

    if $::unifi::install_mongodb {
      include '::apt'

      apt::source { 'mongodb-org-3.4':
        location => 'http://repo.mongodb.org/apt/ubuntu',
        release  => 'xenial/mongodb-org/3.4',
        repos    => 'multiverse',
        key      => {
          id     => '0C49F3730359A14518585931BC711F9BA15703C6',
          server => 'hkp://keyserver.ubuntu.com:80',
        }
      }
    }
  }
  package { 'unifi':
    ensure => $::unifi::ensure,
    name   => $::unifi::package_name,
  }
}

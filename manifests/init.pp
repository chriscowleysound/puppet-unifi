# Class: unifi

class unifi (
  $ensure          = $::unifi::params::ensure,
  $package_name    = $::unifi::params::package_name,
  $java_package    = $::unifi::params::java_package,
  $install_mongodb = $::unifi::params::install_mongodb,
) inherits unifi::params {
  class { '::unifi::install': }
}
